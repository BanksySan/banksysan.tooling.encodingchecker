﻿namespace BanksySan.Tooling.EncodingChecker.Tests.Integration
{
    using System;
    using System.Collections.Concurrent;
    using System.IO;
    using System.Threading.Tasks;
    using EncodingChecker.Checkers;
    using NUnit.Framework;
    using File = FileSystem.File;

    [TestFixture]
    public class RealFilesTests
    {
        private static readonly DirectoryInfo IMAGES_DIRECTORY = Config.TestFileDirectory;
        private static readonly PngFileChecker CHECKER = new PngFileChecker();

        [Test]
        public void  CorrectExtensions()
        {
            var stack = new ConcurrentStack<Result>();

            var files = IMAGES_DIRECTORY.GetFiles();

            Parallel.ForEach(files,
                async file =>
                {
                    var isPng = await CHECKER.Check(new File(file.FullName));
                    stack.Push(new Result { IsPng = isPng, FileName = file.Name });
                });

            Assert.Multiple(() =>
                            {
                                while (!stack.IsEmpty)
                                {
                                    stack.TryPop(out Result result);

                                    var extension = Path.GetExtension(result.FileName);

                                    if (string.Equals(extension, ".png", StringComparison.OrdinalIgnoreCase))
                                    {
                                        Assert.That(result.IsPng, Is.True, $"Filename: {result.FileName}, expected IsPng = true but was {result.IsPng}.");
                                    }
                                    else
                                    {
                                        Assert.That(result.IsPng, Is.False, $"Filename: {result.FileName}, expected IsPng = false but was {result.IsPng}.");
                                    }
                                }
                            });
        }

        class Result
        {
            public bool IsPng { get; set; }
            public string FileName { get; set; }
        }
    }
}