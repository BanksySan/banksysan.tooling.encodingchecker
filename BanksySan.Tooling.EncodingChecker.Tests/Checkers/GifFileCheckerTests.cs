namespace BanksySan.Tooling.EncodingChecker.Tests.Checkers
{
    using System.Threading.Tasks;
    using EncodingChecker.Checkers;
    using FileSystem;
    using Moq;
    using NUnit.Framework;

    public class GifFileCheckerTests
    {
        private static readonly GifFileChecker FILE_CHECKER = new GifFileChecker();
        private static readonly int[] GIF_89_A_SIGNATURE = { 71, 73, 70, 56, 57, 97 };
        private static readonly int[] GIF_87_A_SIGNATURE = { 71, 73, 70, 56, 55, 97 };

        [Test]
        public async Task Correct89ASigniture()
        {
            var mockFileStream = new Mock<IFileStream>();
            var mockFile = new Mock<IFile>();

            mockFile.Setup(x => x.Open()).Returns(mockFileStream.Object);
            mockFileStream.Setup(x => x.ReadByte()).ReturnsInOrder(71, 73, 70, 56, 57, 97, -1);
            var check = await FILE_CHECKER.Check(mockFile.Object);

            Assert.That(check, Is.True);
        }

        [Test]
        public async Task Correct87ASigniture()
        {
            var mockFileStream = new Mock<IFileStream>();
            var mockFile = new Mock<IFile>();

            mockFile.Setup(x => x.Open()).Returns(mockFileStream.Object);
            mockFileStream.Setup(x => x.ReadByte()).ReturnsInOrder(71, 73, 70, 56, 55, 97, -1);
            var check = await FILE_CHECKER.Check(mockFile.Object);

            Assert.That(check, Is.True);
        }

        [Test]
        public async Task IncorrectSignitureTooShort()
        {
            var mockFileStream = new Mock<IFileStream>();
            var mockFile = new Mock<IFile>();

            mockFile.Setup(x => x.Open()).Returns(mockFileStream.Object);
            mockFileStream.Setup(x => x.ReadByte()).ReturnsInOrder(1, 2, 3, -1);
            var check = await FILE_CHECKER.Check(mockFile.Object);

            Assert.That(check, Is.False);
        }

        [Test]
        public async Task IncorrectSignitureWrongBytes()
        {
            var mockFileStream = new Mock<IFileStream>();
            var mockFile = new Mock<IFile>();

            mockFile.Setup(x => x.Open()).Returns(mockFileStream.Object);
            mockFileStream.Setup(x => x.ReadByte()).ReturnsInOrder(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -1);
            var check = await FILE_CHECKER.Check(mockFile.Object);

            Assert.That(check, Is.False);
        }
    }
}