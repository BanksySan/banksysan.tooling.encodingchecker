﻿namespace BanksySan.Tooling.EncodingChecker.Tests
{
    using System.IO;

    static class Config
    {
        public static DirectoryInfo TestFileDirectory => new DirectoryInfo(@"F:\BanksySan\Development\Images");
    }
}