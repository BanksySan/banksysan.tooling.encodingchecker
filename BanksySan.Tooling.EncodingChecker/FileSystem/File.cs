namespace BanksySan.Tooling.EncodingChecker.FileSystem
{
    using System.IO;

    public class File : IFile
    {
        private readonly FileInfo _fileInfo;
        public File(string filePath) : this(new FileInfo(filePath)) { }

        public File(FileInfo fileInfo)
        {
            _fileInfo = fileInfo;
        }

        public IFileStream Open()
        {
            return new FileStream(_fileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
        }
    }
}