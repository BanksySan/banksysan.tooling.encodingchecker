namespace BanksySan.Tooling.EncodingChecker.FileSystem
{
    class FileStream : IFileStream
    {
        private readonly System.IO.FileStream _fileStream;

        public FileStream(System.IO.FileStream fileStream)
        {
            _fileStream = fileStream;
        }

        public int ReadByte()
        {
            return _fileStream.ReadByte();
        }

        public void Dispose()
        {
            _fileStream.Dispose();
        }
    }
}