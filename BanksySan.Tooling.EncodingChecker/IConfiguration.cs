﻿namespace BanksySan.Tooling.EncodingChecker
{
    using System.IO;

    interface IConfiguration
    {
        DirectoryInfo BaseDirectory { get; }
    }
}