﻿namespace BanksySan.Tooling.EncodingChecker
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using Checkers;
    using File = FileSystem.File;

    class ExtensionMapping
    {
        private static readonly IFileChecker BMP_CHECKER = new BmpFileChecker();
        private static readonly IFileChecker GIF_CHECKER = new GifFileChecker();
        private static readonly IFileChecker JPG_CHECKER = new JpgFileChecker();
        private static readonly IFileChecker TIFF_CHECKER = new TiffFileChecker();
        private static readonly IFileChecker PNG_CHECKER = new PngFileChecker();

        private static readonly Dictionary<string, IFileChecker> MAPPINGS;

        static ExtensionMapping()
        {
            MAPPINGS = new Dictionary<string, IFileChecker>(StringComparer.OrdinalIgnoreCase)
                       {
                           { ".bmp", BMP_CHECKER },
                           {
                               ".gif", GIF_CHECKER
                           },
                           {
                               ".jpg", JPG_CHECKER
                           },
                           {
                               ".jpeg", JPG_CHECKER
                           },
                           {
                               ".tiff",
                               TIFF_CHECKER
                           },
                           {
                               ".tif", TIFF_CHECKER
                           },
                           {
                               ".png", PNG_CHECKER
                           }
                       };
        }

        public async Task<IAnalysisResult> TestFile(FileInfo fileInfo)
        {
            if (fileInfo == null)
            {
                return new AnalysisResult { Status = "skipped", Details = "FileInfo is null." };
            }

            if (!fileInfo.Exists)
            {
                return new AnalysisResult { Status = "skipped", Details = $"No file found.", Path = fileInfo.FullName };
            }

            if (!MAPPINGS.ContainsKey(fileInfo.Extension))
            {
                return new AnalysisResult
                       {
                           Status = "skipped",
                           Details = $"Unrecognised extension '{fileInfo.Extension}'.",
                           Path = fileInfo.FullName
                       };
            }

            var success = await MAPPINGS[fileInfo.Extension].Check(new File(fileInfo));

            return new AnalysisResult { Status = success ? "pass" : "failure", Path = fileInfo.FullName };
        }
    }
}