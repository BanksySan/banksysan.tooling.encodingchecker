namespace BanksySan.Tooling.EncodingChecker.Checkers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using FileSystem;

    public class PngFileChecker : IFileChecker
    {
        private static readonly int[] PNG_SIGNATURE = { 137, 80, 78, 71, 13, 10, 26, 10 };

        public Task<bool> Check(IFile file)
        {
            var task = new Task<bool>(() => DoCheck(file));
            task.Start();
            return task;
        }

        private bool DoCheck(IFile file)
        {
            var buffer = new List<int>();

            using (var reader = file.Open())
            {
                for (var i = 0; i < PNG_SIGNATURE.Length; i++)
                {
                    var result = reader.ReadByte();
                    if (result == -1)
                    {
                        break;
                    }

                    buffer.Add(result);
                }
            }

            return buffer.SequenceEqual(PNG_SIGNATURE);
        }
    }
}