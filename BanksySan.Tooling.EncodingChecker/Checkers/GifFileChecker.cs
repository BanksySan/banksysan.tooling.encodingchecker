namespace BanksySan.Tooling.EncodingChecker.Checkers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using FileSystem;

    public class GifFileChecker : IFileChecker
    {
        private static readonly int[] GIF_89_A_SIGNATURE = { 71, 73, 70, 56, 57, 97 };
        private static readonly int[] GIF_87_A_SIGNATURE = { 71, 73, 70, 56, 55, 97 };

        public Task<bool> Check(IFile file)
        {
            var task = new Task<bool>(() => DoCheck(file));
            task.Start();
            return task;
        }

        private bool DoCheck(IFile file)
        {
            var buffer = new List<int>();

            using (var reader = file.Open())
            {
                for (var i = 0; i < GIF_89_A_SIGNATURE.Length; i++)
                {
                    var result = reader.ReadByte();
                    if (result == -1)
                    {
                        break;
                    }

                    buffer.Add(result);
                }
            }

            return buffer.SequenceEqual(GIF_87_A_SIGNATURE) || buffer.SequenceEqual(GIF_89_A_SIGNATURE);
        }
    }
}