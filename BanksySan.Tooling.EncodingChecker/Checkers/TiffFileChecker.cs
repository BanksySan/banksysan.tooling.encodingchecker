namespace BanksySan.Tooling.EncodingChecker.Checkers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using FileSystem;

    public class TiffFileChecker : IFileChecker
    {
        private static readonly int[] TIFF_INTEL_SIGNATURE = { 73, 73, 42 };
        private static readonly int[] TIFF_MOTOROLA_SIGNATURE = { 77, 77, 42 };

        public Task<bool> Check(IFile file)
        {
            var task = new Task<bool>(() => DoCheck(file));
            task.Start();
            return task;
        }

        private bool DoCheck(IFile file)
        {
            var buffer = new List<int>();

            using (var reader = file.Open())
            {
                for (var i = 0; i < TIFF_INTEL_SIGNATURE.Length; i++)
                {
                    var result = reader.ReadByte();
                    if (result == -1)
                    {
                        break;
                    }

                    buffer.Add(result);
                }
            }

            return buffer.SequenceEqual(TIFF_INTEL_SIGNATURE) || buffer.SequenceEqual(TIFF_MOTOROLA_SIGNATURE);
        }
    }
}