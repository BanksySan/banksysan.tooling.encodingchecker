namespace BanksySan.Tooling.EncodingChecker.Checkers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using FileSystem;

    public class JpgFileChecker : IFileChecker
    {
        private static readonly int[] JPG_SIGNITURE = { 255, 216, 255, 224 };

        public Task<bool> Check(IFile file)
        {
            var task = new Task<bool>(() => DoCheck(file));
            task.Start();
            return task;
        }

        private bool DoCheck(IFile file)
        {
            var buffer = new List<int>();

            using (var reader = file.Open())
            {
                for (var i = 0; i < JPG_SIGNITURE.Length; i++)
                {
                    var result = reader.ReadByte();
                    if (result == -1)
                    {
                        break;
                    }

                    buffer.Add(result);
                }
            }

            return buffer.SequenceEqual(JPG_SIGNITURE);
        }
    }
}