﻿namespace BanksySan.Tooling.EncodingChecker
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security;

    struct Configuration : IConfiguration
    {
        private Configuration(DirectoryInfo baseDirectory)
        {
            BaseDirectory = baseDirectory;
        }

        public DirectoryInfo BaseDirectory { get; }

        public static IConfiguration ParseCommandArguments(string[] args)
        {
            if (args == null || args.Length == 0)
            {
                throw new InvalidCommandLineArgumentsException(new[] { "No arguments supplied" });
            }

            var path = args[0];

            DirectoryInfo baseDirectory = null;

            try
            {
                baseDirectory = new DirectoryInfo(path);
            }
            catch (ArgumentNullException)
            {
                throw new InvalidCommandLineArgumentsException(new[] { "No arguments supplied" });
            }
            catch (SecurityException e)
            {
                throw new InvalidCommandLineArgumentsException(new[] { e.Message });
            }
            catch (ArgumentException)
            {
                throw new InvalidCommandLineArgumentsException(new[] { $"Invalid directory path '{path}'." });
            }
            catch (PathTooLongException e)
            {
                throw new InvalidCommandLineArgumentsException(new[] { e.Message });
            }

            if (baseDirectory != null && !baseDirectory.Exists)
            {
                throw new InvalidCommandLineArgumentsException(new[] { $"Cannot find directory at '{path}'." });
            }

            return new Configuration(baseDirectory);
        }
        
    }

    public class InvalidCommandLineArgumentsException : FormatException
    {
        public InvalidCommandLineArgumentsException(IEnumerable<string> errors) : base("Invalid command line arguments")
        {
            Errors = errors;
        }

        public IEnumerable<string> Errors { get; private set; }
    }
}