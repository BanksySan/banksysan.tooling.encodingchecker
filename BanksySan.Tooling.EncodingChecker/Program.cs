﻿namespace BanksySan.Tooling.EncodingChecker
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    class Program
    {
        private static readonly ExtensionMapping EXTENSION_MAPPING = new ExtensionMapping();
        private static readonly ConcurrentBag<IAnalysisResult> RESULTS = new ConcurrentBag<IAnalysisResult>();

        static int Main(string[] args)
        {
            if (args == null || args.Length == 0)
            {
                DisplayWelcomeMessage();
                return 0;
            }

            var config = Configuration.ParseCommandArguments(args);

            var baseDirectory = config.BaseDirectory;
            
            var task =  CheckFileSystemObject(baseDirectory);

            Console.WriteLine("Running");

            while (task.Status == TaskStatus.Running)
            {
                Console.Write(".");
                Thread.Sleep(500);
            }

            task.Wait();

            var succeses = new HashSet<IAnalysisResult>();
            var failures = new HashSet<IAnalysisResult>();

            foreach (var result in RESULTS)
            {
                if (result.Status == "pass")
                {
                    succeses.Add(result);
                }
                else
                {
                    failures.Add(result);
                }
            }

            Console.WriteLine("SUCCESS");
            Console.WriteLine("=======");
            Console.WriteLine();
            foreach (var result in succeses)
            {
                Console.WriteLine("{0, 10}: {1, 30}", result.Status, result.Details);
                Console.WriteLine("\t{0}", result.Path);
            }

            Console.WriteLine("FAILURE");
            Console.WriteLine("=======");
            Console.WriteLine();
            foreach (var result in failures)
            {
                Console.WriteLine("{0, 10}: {1, 30}", result.Status, result.Details);
                Console.WriteLine("\t{0}", result.Path);
            }

            Console.WriteLine();
            Console.WriteLine("Done");
            Console.Read();

            return 0;
        }

        private static void DisplayWelcomeMessage()
        {
            var executingAssembly = System.Reflection.Assembly.GetExecutingAssembly();
            var fileVersionInfo = FileVersionInfo.GetVersionInfo(executingAssembly.Location);
            var version = fileVersionInfo.ProductVersion;

            Console.WriteLine($"Encoding Checker.  Version: {version}");
            Console.WriteLine("Syntax:");
            Console.WriteLine("BanksySan.Tooling.EncodingChecker.exe <directory path>");
        }

        private static async Task CheckFileSystemObject(FileSystemInfo fileSystemInfo)
        {
            if (fileSystemInfo.Attributes.HasFlag(FileAttributes.Directory))
            {
                var directoryInfo = (DirectoryInfo) fileSystemInfo;

                var subFileSystemObjects = directoryInfo.GetFileSystemInfos();

                foreach (var subFileSystemObject in subFileSystemObjects)
                {
                    await CheckFileSystemObject(subFileSystemObject);
                }
            }
            else
            {
                var fileInfo = (FileInfo) fileSystemInfo;

                var result = await EXTENSION_MAPPING.TestFile(fileInfo);

                RESULTS.Add(result);
            }
        }
    }
}