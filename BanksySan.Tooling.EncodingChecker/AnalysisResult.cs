﻿namespace BanksySan.Tooling.EncodingChecker
{
    class AnalysisResult : IAnalysisResult
    {
        public string Status { get; set; }
        public string Details { get; set; }
        public string Path { get; set; }
    }
}